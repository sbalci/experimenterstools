output$line1 <- renderText({
  convertRGB(r = as.numeric(input$iR), g = as.numeric(input$iG), b = as.numeric(input$iB))[1]
})

output$line2 <- renderText({
  convertRGB(r = as.numeric(input$iR), g = as.numeric(input$iG), b = as.numeric(input$iB))[2]
})

output$line3 <- renderText({
  convertRGB(r = as.numeric(input$iR), g = as.numeric(input$iG), b = as.numeric(input$iB))[3]
})

output$patch <- renderPlot({
  c <- rgb2hex(r = as.numeric(input$iR), g = as.numeric(input$iG), b = as.numeric(input$iB))
  par(bg = c)
  plot.new()
})