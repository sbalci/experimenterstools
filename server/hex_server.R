output$line1hex <- renderText({
  convertHex2Rest(ent_hex = input$iHex)[1]
})

output$line2hex <- renderText({
  convertHex2Rest(ent_hex = input$iHex)[2]
})

output$line3hex <- renderText({
  convertHex2Rest(ent_hex = input$iHex)[3]
})

output$patch2 <- renderPlot({
  par(bg = input$iHex)
  plot.new()
})
