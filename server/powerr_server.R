output$p_r_res <- renderText({
  
  cortest_power(nr = as.numeric(input$n_r), rr = as.numeric(input$r_r), sig.levelr = input$a_r, 
                powerr = input$p_r, alternativer = input$hyp_r)
  
})