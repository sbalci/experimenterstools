###############################################################
#####################Visual Angle Function#####################
###############################################################
visualAngle <- function(size, distance, angle){ 
  if (is.na(angle) & is.na(size) | is.na(angle) & is.na(distance) |
      is.na(size) & is.na(distance)){ 
    res <- "Waiting for input - feed me!"
  }
  else if (is.na(angle)){ 
    angle <- 2*atan(size/(2*distance))*(180/pi)
    res <- paste("Visual Angle: ", round(angle, 2))
  } 
  else if (is.na(size)){ 
    size <- 2*distance*tan((angle/(180/pi))/2)
    res <- paste("Object Size: ", round(size, 2))
  } 
  else if (is.na(distance)){ 
    distance <- size/2/tan(angle/(180/pi)/2)
    res <- paste("Distance from Object: ", round(distance, 2))
  }
  else{res <- "That's too much input for me. :("
  }
  return(res)
} 