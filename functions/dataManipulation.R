#############################################
###############SUBSET FUNCTION###############
#############################################
subsetting <- function(df = NULL, var1 = "", var2 = "", var3 = "", var4 = "", op1 = "", op2 = "", op3 = "", op4 = "", cond1 = "", cond2 = "", cond3 = "",
                       cond4 = "", bool1 = "", bool2 = "", bool3 = ""){

  options(useFancyQuotes = FALSE)

  cond1 <- as.character(cond1)
  cond2 <- as.character(cond2)
  cond3 <- as.character(cond3)
  cond4 <- as.character(cond4)

  if(var1 != "" & var2 == "" & var3 == "" & var4 == ""){

    df_arg1 <- paste("df$", var1, sep = "")
    b1 <- df_arg2 <- op2 <- cond2 <- b2 <- df_arg3 <- op3 <- cond3 <- b3 <- df_arg4 <- op4 <- cond4 <- ""

    if(op1=="is"){ op1 <- " == " }
    else if(op1=="is not"){ op1 <- " != " }
    else if(op1=="greater than"){ op1 <- " > " }
    else if(op1=="smaller than"){ op1 <- " < " }

    df_arg <- paste(df_arg1, op1, dQuote(cond1))
    newdf <- eval(parse(text = paste("subset(df,", df_arg, ")")))
  }
  else if(var1 != "" & var2 != "" & var3 == "" & var4 == ""){

    #set all variables that are not needed to "" (nothing)
    var3 <- var4 <- op3 <- op4 <- cond3 <- cond4 <- bool2 <- bool3 <- ""

    df_arg1 <- paste("df$", var1, sep = "")
    df_arg2 <- paste("df$", var2, sep = "")

    if(bool1=="AND"){ b1 <- " & " }
    else if(bool1=="OR"){ b1 <- " | " }

    if(op1=="is"){ op1 <- " == " }
    else if(op1=="is not"){ op1 <- " != " }
    else if(op1=="greater than"){ op1 <- " > " }
    else if(op1=="smaller than"){ op1 <- " < " }

    if(op2=="is"){ op2 <- " == " }
    else if(op2=="is not"){ op2 <- " != " }
    else if(op2=="greater than"){ op2 <- " > " }
    else if(op2=="smaller than"){ op2 <- " < " }

    df_arg <- paste(df_arg1, op1, dQuote(cond1), b1, df_arg2, op2, dQuote(cond2))
    newdf <- eval(parse(text = paste("subset(df,", df_arg, ")")))
  }
  else if(var1 != "" & var2 != "" & var3 != "" & var4 == ""){

    #set all variables that are not needed to "" (nothing)
    var4 <- op4 <- cond4 <- bool3 <- ""

    df_arg1 <- paste("df$", var1, sep = "")
    df_arg2 <- paste("df$", var2, sep = "")
    df_arg3 <- paste("df$", var3, sep = "")

    if(bool1=="AND"){ b1 <- " & " }
    else if(bool1=="OR"){ b1 <- " | " }

    if(bool2=="AND"){ b2 <- " & " }
    else if(bool2=="OR"){ b2 <- " | " }

    if(op1=="is"){ op1 <- " == " }
    else if(op1=="is not"){ op1 <- " != " }
    else if(op1=="greater than"){ op1 <- " > " }
    else if(op1=="smaller than"){ op1 <- " < " }

    if(op2=="is"){ op2 <- " == " }
    else if(op2=="is not"){ op2 <- " != " }
    else if(op2=="greater than"){ op2 <- " > " }
    else if(op2=="smaller than"){ op2 <- " < " }

    if(op3=="is"){ op3 <- " == " }
    else if(op3=="is not"){ op3 <- " != " }
    else if(op3=="greater than"){ op3 <- " > " }
    else if(op3=="smaller than"){ op3 <- " < " }

    df_arg <- paste(df_arg1, op1, dQuote(cond1), b1, df_arg2, op2, dQuote(cond2), b2, df_arg3, op3, dQuote(cond3))
    newdf <- eval(parse(text = paste("subset(df,", df_arg, ")")))
  }
  else if(var1 != "" & var2 != "" & var3 != "" & var4 != ""){
    df_arg1 <- paste("df$", var1, sep = "")
    df_arg2 <- paste("df$", var2, sep = "")
    df_arg3 <- paste("df$", var3, sep = "")
    df_arg4 <- paste("df$", var4, sep = "")

    if(bool1=="AND"){ b1 <- " & " }
    else if(bool1=="OR"){ b1 <- " | " }

    if(bool2=="AND"){ b2 <- " & " }
    else if(bool2=="OR"){ b2 <- " | " }

    if(bool3=="AND"){ b3 <- " & " }
    else if(bool3=="OR"){  b3 <- " | " }

    if(op1=="is"){ op1 <- " == " }
    else if(op1=="is not"){ op1 <- " != " }
    else if(op1=="greater than"){ op1 <- " > " }
    else if(op1=="smaller than"){ op1 <- " < " }

    if(op2=="is"){ op2 <- " == " }
    else if(op2=="is not"){ op2 <- " != " }
    else if(op2=="greater than"){ op2 <- " > " }
    else if(op2=="smaller than"){ op2 <- " < " }

    if(op3=="is"){ op3 <- " == " }
    else if(op3=="is not"){ op3 <- " != " }
    else if(op3=="greater than"){ op3 <- " > " }
    else if(op3=="smaller than"){ op3 <- " < " }

    if(op4=="is"){ op4 <- " == " }
    else if(op4=="is not"){ op4 <- " != " }
    else if(op4=="greater than"){ op4 <- " > " }
    else if(op4=="smaller than"){ op4 <- " < " }

    df_arg <- paste(df_arg1, op1, dQuote(cond1), b1, df_arg2, op2, dQuote(cond2), b2, df_arg3, op3, dQuote(cond3), b3, df_arg4, op4, dQuote(cond4))
    newdf <- eval(parse(text = paste("subset(df,", df_arg, ")")))
  }
  return(newdf)
}


#############################################
################TRIM FUNCTION################
#############################################
trimming <- function(df = NULL, cutlower = "", cutupper = "", method = "", sds = "", measure = "", var1 = "", var2 = "", var3 = "",
                     var4 = "", var5 = "", var6 = "", var7 = "" , dvrt = ""){

  if(method == "cutoff"){

    cut_arg1 <- paste("df$", dvrt, ">", cutlower, sep = "")
    cut_arg2 <- paste("df$", dvrt, "<", cutupper, sep = "")

    cut_arg <- paste(cut_arg1, "&", cut_arg2, sep = " ")
    newdf <- eval(parse(text = paste("subset(df,", cut_arg, ")")))

  }

  else if(method == "SD"){

    if(var1 == ""){cond <- NULL}
    if(var1 != "" & var2 == ""){ cond <- c(var1) }
    if(var1 != "" & var2 != "" & var3 == ""){ cond <- c(var1, var2) }
    if(var1 != "" & var2 != "" & var3 != "" & var4 == ""){ cond <- c(var1, var2, var3) }
    if(var1 != "" & var2 != "" & var3 != "" & var4 != "" & var5 == ""){ cond <- c(var1, var2, var3, var4) }
    if(var1 != "" & var2 != "" & var3 != "" & var4 != "" & var5 != "" & var6 == ""){ cond <- c(var1, var2, var3, var4, var5) }
    if(var1 != "" & var2 != "" & var3 != "" & var4 != "" & var5 != "" & var6 != "" & var7 == ""){ cond <- c(var1, var2, var3, var4, var5, var6) }
    if(var1 != "" & var2 != "" & var3 != "" & var4 != "" & var5 != "" & var6 != "" & var7 != ""){ cond <- c(var1, var2, var3, var4, var5, var6, var7) }


    # Convert character vector to list of symbols
    dots <- lapply(cond, as.symbol)

    if(measure == "mean"){
      df %>% group_by(.dots=dots) %>%
        mutate(meanrt = mean(response_time), sdrt = sd(response_time))-> df}

    if(measure == "median"){
      df %>% group_by(.dots=dots) %>%
        mutate(meanrt = median(response_time), sdrt = sd(response_time))-> df}

    newdf <- eval(parse(text = paste("subset(df, df$", dvrt, ">", "df$meanrt-", sds, "*df$sdrt", "& df$", dvrt, "<", "df$meanrt+", sds, "*df$sdrt", ")", sep = "")))
    newdf$meanrt <- NULL
    newdf$sdrt <- NULL
  }
  return(newdf)
}


######################################################
##################TRANSFORM FUNCTION##################
######################################################
transforming <- function(df = NULL, var1 = "", var2 = "", var3 = "",
                         var4 = "", var5 = "", var6 = "", dvrt = "", dver = "",
                         incorr = "", trans = "", subj = "", medianmean = "", exclude = ""){

  if(exclude=="yes"){
    dfrt <- eval(parse(text = paste("subset(df, df$", dver, "!=", incorr, ")", sep = "")))
  }
  if(exclude=="no"){
    dfrt <- df
  }

  #create aggregate function
  if(var1 == ""){aggregateBy <- paste("c(\"", subj, "\")", sep = "")
                 cnames <- c(subj)}
  if(var1 != "" & var2 == ""){aggregateBy <- paste("c(\"", subj, "\",\"", var1, "\")", sep = "")
                              cnames <- c(subj, var1)}
  if(var1 != "" & var2 != "" & var3 == ""){aggregateBy <- paste("c(\"", subj, "\",\"", var1,"\",\"", var2, "\")", sep = "")
                                           cnames <- c(subj, var1, var2)}
  if(var1 != "" & var2 != "" & var3 != "" & var4 == ""){aggregateBy <- paste("c(\"", subj, "\",\"", var1, "\",\"", var2, "\",\"", var3,
                                                                             "\")", sep = "")
                                                        cnames <- c(subj, var1, var2, var3)}
  if(var1 != "" & var2 != "" & var3 != "" & var4 != "" & var5 == ""){aggregateBy <- paste("c(\"", subj, "\",\"", var1, "\",\"", var2,
                                                                                          "\",\"", var3, "\",\"", var4, "\")", sep = "")
                                                                     cnames <- c(subj, var1, var2, var3, var4)}
  if(var1 != "" & var2 != "" & var3 != "" & var4 != "" & var5 != "" & var6 == ""){aggregateBy <- paste("c(\"", subj, "\",\"", var1,
                                                                                                       "\",\"", var2, "\",\"", var3,
                                                                                                       "\",\"", var4, "\",\"", var5,
                                                                                                       "\")", sep = "")
                                                                                  cnames <- c(subj, var1, var2, var3, var4, var5)}
  if(var1 != "" & var2 != "" & var3 != "" & var4 != "" & var5 != "" & var6 != ""){aggregateBy <- paste("c(\"", subj, "\",\"", var1,
                                                                                                       "\",\"", var2, "\",\"", var3,
                                                                                                       "\",\"", var4, "\",\"", var5,
                                                                                                       "\",\"", var6, "\")", sep = "")
                                                                                  cnames <- c(subj, var1, var2, var3, var4, var5, var6)}

  #create formula for transformation
  if(var1 == ""){argument <- paste(subj, "~","variable", sep = "")}
  if(var1 != "" & var2 == ""){argument <- paste(subj, "~","variable", "+", var1, sep = "")}
  if(var1 != "" & var2 != "" & var3 == ""){argument <- paste(subj, "~","variable", "+", var1, "+", var2, sep = "")}
  if(var1 != "" & var2 != "" & var3 != "" & var4 == ""){argument <- paste(subj, "~","variable", "+", var1, "+", var2, "+", var3, sep = "")}
  if(var1 != "" & var2 != "" & var3 != "" & var4 != "" & var5 == ""){argument <- paste(subj, "~","variable", "+", var1, "+",
                                                                                       var2, "+", var3, "+", var4, sep = "")}
  if(var1 != "" & var2 != "" & var3 != "" & var4 != "" & var5 != "" & var6 == ""){argument <- paste(subj, "~","variable", "+", var1, "+",
                                                                                                    var2, "+", var3, "+", var4, "+", var5, sep = "")}
  if(var1 != "" & var2 != "" & var3 != "" & var4 != "" & var5 != "" & var6 != ""){argument <- paste(subj, "~","variable", "+", var1, "+",
                                                                                                    var2, "+", var3, "+", var4, "+", var5,
                                                                                                    "+", var6, sep = "")}


  if(dvrt!=""){
    if(medianmean=="mean"){
      datart <- eval(parse(text = paste("aggregate(dfrt[,\"", dvrt, "\"], by=dfrt[", aggregateBy, "], mean)", sep = "")))
      colnames(datart) <- c(cnames, "meanRT")
    }
    if(medianmean=="median"){
      datart <- eval(parse(text = paste("aggregate(dfrt[,\"", dvrt, "\"], by=dfrt[", aggregateBy, "], median)", sep = "")))
      colnames(datart) <- c(cnames, "medianRT")
    }
    c<-melt(datart, id.vars = cnames)
    newdf <- dcast(c, argument)
  }

  if(dver!=""){
    dataer <- eval(parse(text = paste("aggregate(df[,\"", dver, "\"], by=df[", aggregateBy, "], mean)", sep = "")))
    colnames(dataer) <- c(cnames, "meanErrorRate")
    dataer$meanErrorRate <- 1-(dataer$meanErrorRate)

    if(dvrt=="" & dver!=""){
      newdat <- dataer
      colnames(newdat) <- c(cnames, "meanErrorRate")
    }

    if(dvrt!="" & dver!=""){
      newdat <- cbind(datart, dataer$meanErrorRate)
      colnames(newdat) <- c(colnames(datart), "meanErrorRate")
    }


    if(trans=="yes"){
      newdat$asin_acc <- asin(sqrt(newdat$meanErrorRate))
    }

    newdat$meanErrorRate <- newdat$meanErrorRate*100

    c<-melt(newdat, id.vars = cnames)
    newdf <- dcast(c, argument)
  }

  return(newdf)
}

data_loss <- function(df1, df2){
  100-((nrow(df2)/nrow(df1))*100)
}
