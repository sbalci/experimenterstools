ui<-fluidPage(
  
  setBackgroundColor(color = rgb(200, 200, 200, maxColorValue = 255)),
  
  title = "Experimenter's Tools",
  
  div(style = "text-align: center;"),
  
  h2("Welcome to the Experimenter's Toolbox", align = "center"),
  
  p("This application's aim is to provide easy to use tools that are frequently used in basic psychological research in an easy to use manner. 
    While there are already several great websites offering useful tools, this ShinyApp is designed to gather the various functions
    in only one place. The functions on this site are written in R and can easly be copied and adapted, e.g., in RStudio. However,
    in some cases, equivalents of functions are also provided in Python. Whenever functions from other
    authors are used, it is made visible so you can check out their packages yourself. This page will be permanently updated and (hopefully) improved. 
    If you have any suggestions regarding functions you would like to find here,
    please ", a("contact me" ,href="mailto:christian.buesel@uibk.ac.at?"), " with your requests. Of course, also contributions are very welcome!", align = "center"),
  hr(),
  
  
  p("You can view the source code and contribute on: ", h5(icon(name = "gitlab"), a("Gitlab", href="https://gitlab.com/chbuesel/experimenterstools"))), 
  hr(),
  h3("What the Experimenters' Toolbox is and what it is not"), br(),
  h4("What it is"),
  p("As described above, the Experimenters' Toolboxes main aim is to collect and share useful tools for experimental psychological research.
    This site hopefully grows and grows and provides as many researchers and students as possible with an easy to understand interface for commonly and 
    frequently used tasks.", br(),
    "While I do not deny not having an impeccable sense of style - let alone the programming skills -, the easy and unembellished design of this page 
    puts the readability of the source code in the focus. In addition, contributions to this site should not be made unnecessarily hard due to - essentially -
    not all too important issues, such as fanciness.", br(),
    "At some point, I would love to include a platform on which helpful video tutorials or lectures (e.g., from YouTube) can be shared. However, this is
    not even close to realizaion. If you are motivated or have material that you want to share with students and colleagues, however, feel free to contribute!"), br(), br(),
  h4("What it is not"),
  p("We do not need to reinvent the wheel. Hence, this page is not intended to be some sort of web-based data analysis tool. There are already many awesome
    free or open source programs out there that make data analyses way easier than I could ever hope to. For example, ", tags$a("JASP", href="https://jasp-stats.org/"),
    "and", tags$a("JAMOVI", href="https://www.jamovi.org/"), "are awesome and free alternatives to SPSS, if you wish to do statistics with a Graphical User Interface.", br())
  
  )

