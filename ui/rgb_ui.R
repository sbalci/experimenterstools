fluidPage(
  
  tags$style(type="text/css",
             ".shiny-output-error { visibility: hidden; }",
             ".shiny-output-error:before { visibility: hidden; }"),
  
  
  setBackgroundColor(color = rgb(200, 200, 200, maxColorValue = 255)),
  
  h4("Convert RGB values to Hex and HSV", align="center"),
  p("Enter your RGB values in the respective boxes. The Hex and HSV will be displayed below in the Results.",
    "Alternatively, you can copy-and-paste the R- or Python-Function provided below in your preferred IDE", align="center"),
  br(),
  hr(),
  
  fluidRow(
    
    column(3, textInput(inputId = "iR", label = "R-Value", placeholder = "0 - 255", value = NULL)),
    
    column(3, textInput(inputId = "iG", label = "G-Value", placeholder = "0 - 255", value = NULL)),
    
    column(3, textInput(inputId = "iB", label = "B-Value", placeholder = "0 - 255", value = NULL))),
  
  br(),
  hr(),
  
  h4("Result:"),
  textOutput("line1"),
  textOutput("line2"),
  textOutput("line3"),
  
  br(),
  
  plotOutput("patch"),
  
  br(),
  
  hr(),
  
  h4(icon(name = "r-project"),"-Function"),
  
  code("newrgb2hsv <- function(r, g, b){", br(),
       HTML('&emsp;'), "minc = min( r, g, b )", br(), 
       HTML('&emsp;'), "maxc = max( r, g, b )", br(),
       HTML('&emsp;'), "v = maxc/255", br(),                       
       HTML('&emsp;'), "delta = maxc - minc", br(),
       HTML('&emsp;'), "if( maxc != 0 ){", br(), 
       HTML('&emsp;'),HTML('&emsp;'), "s = delta / maxc", br(),
       HTML('&emsp;'),"}", br(),
       HTML('&emsp;'), "else {", br(),                        
       HTML('&emsp;'),HTML('&emsp;'),"s = 0", br(),
       HTML('&emsp;'),HTML('&emsp;'),"h = -1", br(),
       HTML('&emsp;'), "}", br(),
       HTML('&emsp;'), "if (maxc == minc) {", br(),                
       HTML('&emsp;'),HTML('&emsp;'),"h = 0", br(),
       HTML('&emsp;'),HTML('&emsp;'),"s = 0", br(),
       HTML('&emsp;'), "}", br(),
       HTML('&emsp;'), "if( r == maxc ){", br(),
       HTML('&emsp;'),HTML('&emsp;'),"h = ( g - b ) / delta", br(),
       HTML('&emsp;'), "}", br(),
       HTML('&emsp;'), "else if( g == maxc ){", br(),
       HTML('&emsp;'),HTML('&emsp;'), "h = 2 + ( b - r ) / delta", br(),
       HTML('&emsp;'), "}", br(),
       HTML('&emsp;'), "else{", br(),
       HTML('&emsp;'),HTML('&emsp;'),"h = 4 + ( r - g ) / delta", br(),   
       HTML('&emsp;'),HTML('&emsp;'),"h = h * 60", br(),
       HTML('&emsp;'),"}", br(),                     
       HTML('&emsp;'), "if( h < 0 ){", br(),
       HTML('&emsp;'),HTML('&emsp;'), "h = h + 360", br(),
       HTML('&emsp;'),"}", br(),
       HTML('&emsp;'), "res <- c(h, s, v)", br(),
       HTML('&emsp;'), "return(res)", br(),
       "}"), br(),
  "This code was adapted from: https://www.rapidtables.com/convert/color/rgb-to-hsv.html",
  
  
  "Or alternatively...", br(),
  code("library(\"grDevices\")", br(),
       "rgb2hsv(r, g = NULL, b = NULL, maxColorValue = 255)"),
  
  h4(icon(name = "python"),"-Function"),
  
  code("coming soon...")
)