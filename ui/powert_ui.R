fluidPage(
  
  tags$style(type="text/css",
             ".shiny-output-error { visibility: hidden; }",
             ".shiny-output-error:before { visibility: hidden; }"
  ),
  
  setBackgroundColor(color = rgb(200, 200, 200, maxColorValue = 255)),
  
  withMathJax(),
  
  h4("Power Analysis for t-Test"),
  p("This page allows you to perform power analyses for t-tests. In order for it to work, you have to specify all but one value.
    That is, if you want to determine your necessary sample size or the required size of your effect, you just leave the respective field
    empty. If you want to determine the power (1 - \\( \\beta\\)) of your study or (interestingly) the \\( \\alpha \\) - level, you have to set the respective slider input
    to 0.", br(), "The underlying functions performing the power analysis was adapted from ", a("the pwr-package", href="https://cran.r-project.org/web/packages/pwr/"),
    "by Champely and colleagues. It is a easy to use and helpful R-package that makes power analyses really simple."),
  
  fluidRow(
    
    #input for N
    
    column(5, textInput("n_t", label = h5("Sample Size"), value = NULL)),
    
    #input for d
    
    column(5, textInput("d_t", label = h5("Cohen's d"), value = NULL)),
    
    #input for a
    
    column(5, sliderInput("a_t", label = h5("\\( \\alpha \\) - Level"), min = 0, max = 1, value = 0.05)),
    
    #input for power
    
    column(5, sliderInput("p_t", label = h5("Power (1 - \\( \\beta \\))"), min = 0, max = 1, value = 0.80))),
  
  
  #specify whether paired or not
  
  radioButtons("type",
               label = "Paired Sample?",
               choices = list("two samples" = "two.sample","paired" = "paired", "one sample" = "one.sample"),
               selected = "two.sample"),
  
  #specify hypothesis
  
  radioButtons("hyp_t",
               label = "Alternative Hypothesis",
               choices = list("two-sided" = "two.sided","greater" = "greater", "less" = "less"),
               selected = "two.sided"),
  
  tags$b("Result:", style = "font-size:20px"),  
  #output text
  textOutput("p_t_res"),
  
  hr(),
  
  h4(icon("r-project"), "-Code"), br(),
  
  code("#if you haven't already installed the package, run:", br(),"install.packages(\"pwr\")" ,br(),
       "library(pwr)", br(),
       "pwr.t.test(n = NULL, d = NULL, sig.level = 0.05, power = NULL,", br(),  HTML('&emsp;'), 
       "type = c(\"two.sample\", \"one.sample\", \"paired\"),", br(), HTML('&emsp;'),
       "alternative = c(\"two.sided\", \"greater\", \"less\"))"),
  
  hr(),
  
  p("Stephane Champely (2018). pwr: Basic Functions for Power Analysis. R package version 1.2-2. https://CRAN.R-project.org/package=pwr")
  
  )